<?php

namespace App\Http\Controllers;

use App\Models\Table;
use Facade\Ignition\Tabs\Tab;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class TableController extends Controller
{
    public function index(){
        $tables = Table::get();
            return view('table.index')->with('tables', $tables);
    }

    public function createTable(Request $request){
        $validator = Validator::make($request->all(), [
            'minimum_capacity' => 'required|integer|min:1|max:100',
            'maximum_capacity' => 'required|integer|max:200'
        ]);

        if ($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $minimum = $request->minimum_capacity;
        $maximum = $request->maximum_capacity;

        if ($maximum <= $minimum){
            return redirect()->back()->withErrors(['bad_request' => __('Error_Bad_Request_Table')])->withInput($request->all());
        }

        try {
          DB::beginTransaction();
          $table = new Table();
          $table->minimum_capacity = $minimum;
          $table->maximum_capacity = $maximum;
          $table->save();
          DB::commit();
          return redirect()->back()->with('alert-success', __('Create_Table_Correct'));

        }catch (\Exception $e){
            DB::rollBack();
            return redirect()->back()->withErrors(['bad_request' => __('Catch_Create_Table')])->withInput($request->all());
        }
    }

    public function deleteTable(Request $request){
        $validator = Validator::make($request->all(), [
            'table_id' => 'required|integer',
        ]);

        if ($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $table = Table::find($request->table_id);
        if(!$table){
            return redirect()->back()->withErrors(['bad_request' => __('Catch_Delete_Table')])->withInput($request->all());
        }

        try{
            Table::where('id', $request->table_id)->first()->delete();
            return redirect()->back()->with('alert-success', __('Delete_Table_Correct'));
        }catch(\Exception $e){
            return redirect()->back()->withErrors(['bad_request' => __('Catch_Delete_Table')])->withInput($request->all());
        }

    }
}
