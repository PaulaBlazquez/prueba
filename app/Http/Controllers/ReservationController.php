<?php

namespace App\Http\Controllers;

use App\Models\Reservation;
use App\Models\Table;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ReservationController extends Controller
{
    public function index(){
        $reservations = Reservation::get();
        return view('reservation.index')->with('reservations', $reservations);
    }

    public function checkDisponibility(Request $request){
        $validator = Validator::make($request->all(), [
            'check_date' => 'required|date',
            'check_person' => 'required|integer|max:200'
        ]);
        if ($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }
        if ($request->check_date < Carbon::yesterday()){
            return redirect()->back()->withErrors(['bad_date' => __('Error_Bad_Date')])->withInput($request->all());
        }

        $date = $request->check_date;

        $table = Table::where('minimum_capacity', '<=', $request->check_person)
            ->where('maximum_capacity', '>=', $request->check_person)
            ->whereDoesntHave('reservation', function ($q) use ($date){
                $q->where('date', "=", $date);
            })
            ->get();

        return view('reservation.index', ['filtered_table' => $table]);
    }

    public function createReservation(Request $request){
        $validator = Validator::make($request->all(), [
            'reservation_date' => 'required|date',
            'people_reservation' => 'required|integer|max:200|min:1',
            'table_id' => 'required|integer',
            'client_name' => 'required|string|max:20'
        ]);
        if ($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }
        if ($request->reservation_date < Carbon::yesterday()){
            return redirect()->back()->withErrors(['bad_date' => __('Error_Bad_Date')])->withInput($request->all());
        }
        $table = Table::find($request->table_id);
        if(!$table){
            return redirect()->back()->withErrors(['bad_request' => __('Catch_Incorrect_Table')])->withInput($request->all());
        }
        $date = $request->reservation_date;

        $reservation_table = Table::where('id', $request->table_id)->where('minimum_capacity', '<=', $request->people_reservation)
            ->where('maximum_capacity', '>=', $request->people_reservation)
            ->whereDoesntHave('reservation', function ($q) use ($date){
                $q->where('date', "=", $date);
            })
            ->get();

        if(!$reservation_table){
            return redirect()->back()->withErrors(['bad_request' => __('Catch_Table_Reservation')])->withInput($request->all());
        }

        try {
            DB::beginTransaction();
            $reservation = new Reservation();

            $reservation->date = $request->reservation_date;
            $reservation->people = $request->people_reservation;
            $reservation->client_name = $request->client_name;
            $reservation->fk_table_id = $request->table_id;
            $reservation->client_id = substr(md5(time()), 0, 10);

            $reservation->save();
            DB::commit();
            //return redirect(url('manage_reservations'))->with('creation_number', $reservation->client_id);
            return view('reservation.index', ['creation_number' => $reservation->client_id])->with('reservations', Reservation::get());

        }catch (\Exception $e){
            DB::rollBack();
            return redirect()->back()->withErrors(['bad_request' => __('Catch_Create_Reservation') . $e])->withInput($request->all());
        }
    }

    public function deleteReservation(Request $request){
        $validator = Validator::make($request->all(), [
            'reservation_public_id' => 'required',
        ]);

        if ($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $reservation = Reservation::where('client_id', strtolower($request->reservation_public_id))->first();
        if(!$reservation){
            return redirect()->back()->withErrors(['bad_request' => __('Catch_Incorrect_Reservation')])->withInput($request->all());
        }
        $reservation->delete();
        return redirect()->back()->with('alert-success', __('Delete_Reservation_Correct'));
    }
}
