<?php

namespace App\Http\Controllers\API;

use App\Models\Reservation;
use App\Models\Table;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\BaseController;

class APIReservationController extends BaseController
{

    public function checkDisponibility(Request $request){

        $validator = Validator::make($request->all(), [
            'check_date' => 'required|date',
            'check_person' => 'required|integer|max:200'
        ]);
        if ($validator->fails()){
            return $this->sendError(["message" => "Hay errores en el formulario", "errors" => $validator->errors()->toArray()]);
        }

        $date = Carbon::parse($request->check_date)->format('Y-m-d');
        if ($date < Carbon::yesterday()){
            return $this->sendError(__('Error_Bad_Date'));
        }

        $table = Table::where('minimum_capacity', '<=', $request->check_person)
            ->where('maximum_capacity', '>=', $request->check_person)
            ->whereDoesntHave('reservation', function ($q) use ($date){
                $q->where('date', "=", $date);
            })
            ->get();

        return $this->sendResponse($table->toArray(), __('Disponibility_Tables'));
    }

    public function createReservation(Request $request){
        $validator = Validator::make($request->all(), [
            'reservation_date' => 'required|date',
            'people_reservation' => 'required|integer|max:200|min:1',
            'table_id' => 'required|integer',
            'client_name' => 'required|string|max:20'
        ]);
        if ($validator->fails()){
            return $this->sendError(["message" => "Hay errores en el formulario", "errors" => $validator->errors()->toArray()]);
        }

        $date =  Carbon::parse($request->check_date)->format('Y-m-d');;
        if ($date < Carbon::yesterday()){
            return $this->sendError(__('Error_Bad_Date'));
        }
        $table = Table::find($request->table_id);
        if(!$table){
            return $this->sendError(__('Catch_Incorrect_Table'));
        }

        $reservation_table = Table::where('id', $request->table_id)->where('minimum_capacity', '<=', $request->people_reservation)
            ->where('maximum_capacity', '>=', $request->people_reservation)
            ->whereDoesntHave('reservation', function ($q) use ($date){
                $q->where('date', "=", $date);
            })
            ->get();

        if(!$reservation_table){
            return $this->sendError(__('Catch_Table_Reservation'));
        }

        try {
            DB::beginTransaction();
            $reservation = new Reservation();

            $reservation->date = $request->reservation_date;
            $reservation->people = $request->people_reservation;
            $reservation->client_name = $request->client_name;
            $reservation->fk_table_id = $request->table_id;
            $reservation->client_id = substr(md5(time()), 0, 10);

            $reservation->save();
            DB::commit();
            return $this->sendResponse($reservation->client_id, __('Create_Reservation_Correct'));

        }catch (\Exception $e){
            DB::rollBack();
            return $this->sendError(__('Catch_Create_Reservation'));
        }
    }

    public function deleteReservation(Request $request){
        $validator = Validator::make($request->all(), [
            'reservation_public_id' => 'required',
        ]);

        if ($validator->fails()){
            return $this->sendError(["message" => "Hay errores en el formulario", "errors" => $validator->errors()->toArray()]);
        }

        $reservation = Reservation::where('client_id', strtolower($request->reservation_public_id))->first();
        if(!$reservation){
            return $this->sendError(__('Catch_Incorrect_Reservation'));
        }
        $reservation->delete();
        return $this->sendResponse($reservation, __('Delete_Reservation_Correct'));
    }
}
