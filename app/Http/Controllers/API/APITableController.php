<?php

namespace App\Http\Controllers\API;

use App\Models\Table;
use Facade\Ignition\Tabs\Tab;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\BaseController;

class APITableController extends BaseController
{
    public function createTable(Request $request){
        $validator = Validator::make($request->all(), [
            'minimum_capacity' => 'required|integer|min:1|max:100',
            'maximum_capacity' => 'required|integer|max:200'
        ]);

        if ($validator->fails()){
            return $this->sendError(["message" => "Hay errores en el formulario", "errors" => $validator->errors()->toArray()]);
        }

        $minimum = $request->minimum_capacity;
        $maximum = $request->maximum_capacity;

        if ($maximum <= $minimum){
            return $this->sendError(__('Error_Bad_Request_Table'));
        }


        try {
          DB::beginTransaction();
          $table = new Table();
          $table->minimum_capacity = $minimum;
          $table->maximum_capacity = $maximum;
          $table->save();
          DB::commit();
          return $this->sendResponse($table->toArray(), __('Create_Table_Correct'));

        }catch (\Exception $e){
            DB::rollBack();
            return $this->sendError(__('Catch_Create_Table'));
        }
    }

    public function deleteTable(Request $request){
        $validator = Validator::make($request->all(), [
            'table_id' => 'required|integer',
        ]);

        if ($validator->fails()){
            $error = $validator->errors()->first();
            return $this->sendError($error);
        }

        $table = Table::find($request->table_id);
        if(!$table){
            return $this->sendError(__('Catch_Delete_Table'));
        }

        try{
            Table::where('id', $request->table_id)->first()->delete();
            return $this->sendResponse($table->toArray(), __('Delete_Table_Correct'));
        }catch(\Exception $e){
            return $this->sendError(__('Catch_Delete_Table'));
        }

    }
}
