<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Table extends Model
{
    use SoftDeletes;
    protected $table = 'tables';
    public $timestamps = true;
    protected $fillable = array('minimum_capacity', 'maximum_capacity');

    public function reservation(){
        return $this->hasMany('App\Models\Reservation', 'fk_table_id', 'id');
    }
}
