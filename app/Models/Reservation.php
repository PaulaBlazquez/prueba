<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reservation extends Model
{
    use SoftDeletes;
    protected $table = 'reservations';
    public $timestamps = true;
    protected $fillable = array('date', 'people', 'client_name', 'client_id', 'fk_table_id');

    public function table(){
        return $this->belongsTo('App\Models\Reservation', 'fk_table_id');
    }
}
