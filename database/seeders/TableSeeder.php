<?php

namespace Database\Seeders;

use App\Models\Table;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session as SessionLaravel;

class TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try{
            DB::beginTransaction();
            for ($i = 0; $i < 2; $i++){
                $table = new Table();
                $table->minimum_capacity = 1;
                $table->maximum_capacity = 2;
                $table->save();
            }
            for ($i = 0; $i < 2; $i++){
                $table = new Table();
                $table->minimum_capacity = 3;
                $table->maximum_capacity = 4;
                $table->save();
            }
            DB::commit();

        }catch(\Exception $e){
            DB::rollBack();
            var_dump($e);
        }
    }
}
