@extends('layout/public_template')

@section('content')
    <div class="row m-0">
        <div class="col-md-12">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                @endif
            @endforeach
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>
    <div class="row m-0 d-flex align-items-center">
        <div class="col-md-12 text-center p-0">
            <h1>{{__('Tables')}}</h1>
        </div>
    </div>

    <div class="row d-flex align-items-center justify-content-center m-0">
        <div class="col-md-3 box-table">
            <a href="#" class="btn btn-block btn-warning d-flex align-items-center justify-content-center" data-toggle="modal" data-target="#create-table-modal"><img
                    src="{{asset('img/plus.svg')}}" class="" alt="">{{__('Create_Table')}}</a>
        </div>
        <div class="col-md-3 box-table">
            <a href="#" class="btn btn-block btn-warning d-flex align-items-center justify-content-center" data-toggle="modal" data-target="#delete-table-modal"><img
                    src="{{asset('img/trash.svg')}}" alt="">{{__('Delete_Table')}}</a>
        </div>
    </div>
    <div class="modal fade" id="create-table-modal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <form action="{{url('create_table')}}" method="post" class="p-2">
                    @csrf
                    <div class="row m-0">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="minimum_capacity" class="w-100 text-center">{{__('Minimum_Capacity')}}</label>
                                <input type="number" value="{{@old('minimum_capacity')}}" class="form-control" name="minimum_capacity" id="minimum_capacity">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="maximum_capacity" class="w-100 text-center">{{__('Maximum_Capacity')}}</label>
                                <input type="number" value="{{@old('maximum_capacity')}}" class="form-control" name="maximum_capacity" id="maximum_capacity">
                            </div>
                        </div>
                    </div>
                    <div class="row m-0">
                        <button class="col-md-12 btn btn-block btn-info" type="Submit">
                            {{__('Send')}}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="delete-table-modal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <form action="{{url('delete_table')}}" method="post" class="p-2">
                    @csrf
                    <div class="row m-0">
                        <div class="col-md-12 p-0">
                            <div class="form-group">
                                <label for="table_id" class="w-100 text-center">{{__('Table_Id')}}</label>
                                <input type="number" value="{{@old('table_id')}}" class="form-control" name="table_id" id="table_id">
                            </div>
                        </div>
                        <button class="col-md-12 btn btn-block btn-info" type="Submit">
                            {{__('Send')}}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <table class="table table-bordered table-striped" id="tables" cellspacing="0">
        <thead>
            <tr>
                <th>{{__('Id')}}</th>
                <th>{{__('Minimum_Capacity')}}</th>
                <th>{{__('Maximum_Capacity')}}</th>
            </tr>
        </thead>
        <tbody>
            @foreach($tables as $t)
                <tr>
                    <td>{{$t->id}}</td>
                    <td>{{$t->minimum_capacity}}</td>
                    <td>{{$t->maximum_capacity}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <script>
        $('#tables').DataTable({
            'paging'      : true,
            'lengthChange': true,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : true,
            'processing': true,
            'language': {
                'sProcessing': 'Procesando...',
                'sLengthMenu': 'Mostrar _MENU_',
                'sZeroRecords': 'No se encontraron mesas',
                'sEmptyTable': 'No se encontró ninguna mesa.',
                'sInfo': 'Mostrando del <b>_START_ al _END_</b> de un total de <b>_TOTAL_</b> mesas',
                'sInfoEmpty': '',
                'sInfoFiltered': '(filtrado de un total de _MAX_ mesa)',
                'sInfoPostFix': '',
                'sSearch': 'Buscar:',
                'sUrl': '',
                'sInfoThousands': ',',
                'sLoadingRecords': 'Cargando...',
                'oPaginate': {
                    'sFirst': 'Primero',
                    'sLast': 'Último',
                    'sNext': 'Siguiente',
                    'sPrevious': 'Anterior'
                },
                'oAria': {
                    'sSortAscending': ': Activar para ordenar la columna de manera ascendente',
                    'sSortDescending': ': Activar para ordenar la columna de manera descendente'
                }
            },
        })
    </script>
@endsection
