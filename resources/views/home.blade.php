@extends('layout/public_template')
@section('content')
    <div class="container-fluid d-flex justify-content-center align-items-center p-0 custom-container">
        <div class="row w-100 m-0 d-flex align-content-center justify-content-center">
            <div class="col-md-3 box">
                <a href="{{url('manage_tables')}}" class="btn btn-block btn-warning d-flex align-items-center justify-content-center">
                    <img src="{{asset('img/table.svg')}}" alt="Table Image">
                    {{__('Tables')}}
                </a>
            </div>
            <div class="col-md-3 box">
                <a href="{{url('manage_reservations')}}" class="btn btn-block btn-warning d-flex align-items-center justify-content-center">
                    <img src="{{asset('img/reservation.svg')}}" alt="Table Image">
                    {{__('Reservations')}}
                </a>
            </div>
        </div>
    </div>

@endsection
