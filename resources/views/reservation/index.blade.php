@extends('layout/public_template')

@section('content')
    <div class="row m-0">
        <div class="col-md-12">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                @endif
            @endforeach
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>
    @if(isset($filtered_table))
       <div class="row m-0 d-flex align-items-center">
            <div class="col-md-12 text-center p-0">
                <h1><a href="{{url('/manage_reservations')}}" class="custom-ancle text-dark">{{__('Back_Reservation')}}</a></h1>
            </div>
        </div>
    @else
        <div class="row m-0 d-flex align-items-center">
            <div class="col-md-12 text-center p-0">
                <h1>{{__('Reservations')}}</h1>
            </div>
            <div class="col-md-2">

            </div>
        </div>
    @endif


    <div class="row d-flex align-items-center justify-content-center m-0">
        <div class="col-md-3 box-table">
            <a href="#" class="btn btn-block btn-warning d-flex align-items-center justify-content-center" data-toggle="modal" data-target="#disponibility-modal"><img
                    src="{{asset('img/find.svg')}}" alt="">{{__('Table_Disponibility')}}</a>
        </div>
        <div class="col-md-3 box-table">
            <a href="#" class="btn btn-block btn-warning d-flex align-items-center justify-content-center" data-toggle="modal" data-target="#create-reservation-modal"><img
                    src="{{asset('img/plus.svg')}}" alt="">{{__('Create_Reservation')}}</a>
        </div>
        <div class="col-md-3 box-table">
            <a href="#" class="btn btn-block btn-warning d-flex align-items-center justify-content-center" data-toggle="modal" data-target="#delete-reservation-modal"><img
                    src="{{asset('img/trash.svg')}}" alt="">{{__('Delete_Reservation')}}</a>
        </div>
    </div>
    @if(isset($creation_number))
        <div class="row">
            <div class="col-md-8 offset-sm-2 rounded custom-message">
                <p class=" h-100 d-flex align-items-center justify-content-center">
                    {{__('Reservation_Identifier')}} {{$creation_number}}
                </p>
            </div>
        </div>
    @endif
    <div class="modal fade" id="disponibility-modal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <form action="{{url('check_disponibility')}}" method="post" class="p-3">
                    @csrf
                    <div class="row m-0">
                        <div class="col-md-12 p-0">
                            <div class="form-group">
                                <label for="check_date" class="w-100 text-center">{{__('Check_Date')}}</label>
                                <input type="date" value="{{@old('check_date')}}" class="form-control" name="check_date" id="check_date">
                            </div>
                        </div>
                        <div class="col-md-12 p-0">
                            <div class="form-group">
                                <label for="check_person" class="w-100 text-center">{{__('Check_People')}}</label>
                                <input type="number" value="{{@old('check_person')}}" class="form-control" name="check_person" id="check_person">
                            </div>
                        </div>
                        <button class="col-md-12 btn btn-block btn-info" type="Submit">
                            {{__('Send')}}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="create-reservation-modal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <form action="{{url('create_reservation')}}" method="post" class="p-3">
                    @csrf
                    <div class="row m-0">
                        <div class="col-md-12 p-0">
                            <div class="form-group">
                                <label for="reservation_date" class="w-100">{{__('Date_Reservation')}}</label>
                                <input type="date" value="{{@old('reservation_date')}}" class="form-control" name="reservation_date" id="reservation_date">
                            </div>
                        </div>
                        <div class="col-md-12 p-0">
                            <div class="form-group">
                                <label for="people_reservation" class="w-100">{{__('People_Reservation')}}</label>
                                <input type="number" value="{{@old('people_reservation')}}" class="form-control" name="people_reservation" id="people_reservation">
                            </div>
                        </div>
                        <div class="col-md-12 p-0">
                            <div class="form-group">
                                <label for="table_id" class="w-100">{{__('Table_Reservation')}}</label>
                                <input type="number" value="{{@old('table_id')}}" class="form-control" name="table_id" id="table_id">
                            </div>
                        </div>
                        <div class="col-md-12 p-0">
                            <div class="form-group">
                                <label for="client_name" class="w-100">{{__('Client_Name')}}</label>
                                <input type="text" value="{{@old('client_name')}}" class="form-control" name="client_name" id="client_name">
                            </div>
                        </div>
                    </div>
                    <div class="row m-0">
                        <button class="col-md-12 btn btn-block btn-info" type="Submit">
                            {{__('Send')}}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="delete-reservation-modal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <form action="{{url('delete_reservation')}}" method="post" class="p-3">
                    @csrf
                    <div class="row m-0">
                        <div class="col-md-12 p-0">
                            <div class="form-group">
                                <label for="reservation_public_id" class="w-100 text-center">{{__('Reservation_Id')}}</label>
                                <input type="text" value="{{@old('reservation_public_id')}}" class="form-control" name="reservation_public_id" id="reservation_public_id">
                            </div>
                        </div>
                        <button class="col-md-12 btn btn-block btn-info" type="Submit">
                            {{__('Send')}}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @if(isset($filtered_table))
        <h2>{{__('Disponibility_Tables')}}</h2>
        <table class="table table-bordered table-striped" id="available_tables" cellspacing="0">
            <thead>
                <tr>
                    <th>{{__('Id')}}</th>
                    <th>{{__('Minimum_Capacity')}}</th>
                    <th>{{__('Maximum_Capacity')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($filtered_table as $tf)
                    <tr>
                        <td>{{$tf->id}}</td>
                        <td>{{$tf->minimum_capacity}}</td>
                        <td>{{$tf->maximum_capacity}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @else
        <table class="table table-bordered table-striped" id="reservations" cellspacing="0">
            <thead>
                <tr>
                    <th>{{__('Date_Reservation')}}</th>
                    <th>{{__('People_Reservation')}}</th>
                    <th>{{__('Client_Name')}}</th>
                    <th>{{__('Table_Reservation')}}</th>
                    <th>{{__('Public_Reservation_Id')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($reservations as $r)
                    <tr>
                        <td>{{date('d-m-Y', strtotime($r->date))}}</td>
                        <td>{{$r->people}}</td>
                        <td>{{$r->client_name}}</td>
                        <td>{{$r->fk_table_id}}</td>
                        <td>{{$r->client_id}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif

    <script>
        $('#reservations').DataTable({
            'paging'      : true,
            'lengthChange': true,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : true,
            'processing': true,
            'language': {
                'sProcessing': 'Procesando...',
                'sLengthMenu': 'Mostrar _MENU_',
                'sZeroRecords': 'No se encontraron reservas',
                'sEmptyTable': 'No se encontró ninguna reserva.',
                'sInfo': 'Mostrando del <b>_START_ al _END_</b> de un total de <b>_TOTAL_</b> reservas',
                'sInfoEmpty': '',
                'sInfoFiltered': '(filtrado de un total de _MAX_ reservas)',
                'sInfoPostFix': '',
                'sSearch': 'Buscar:',
                'sUrl': '',
                'sInfoThousands': ',',
                'sLoadingRecords': 'Cargando...',
                'oPaginate': {
                    'sFirst': 'Primero',
                    'sLast': 'Último',
                    'sNext': 'Siguiente',
                    'sPrevious': 'Anterior'
                },
                'oAria': {
                    'sSortAscending': ': Activar para ordenar la columna de manera ascendente',
                    'sSortDescending': ': Activar para ordenar la columna de manera descendente'
                }
            },
        });

        $('#available_tables').DataTable({
            'paging'      : true,
            'lengthChange': true,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : true,
            'processing': true,
            'language': {
                'sProcessing': 'Procesando...',
                'sLengthMenu': 'Mostrar _MENU_',
                'sZeroRecords': 'No se encontraron mesas',
                'sEmptyTable': 'No se encontró ninguna mesa.',
                'sInfo': 'Mostrando del <b>_START_ al _END_</b> de un total de <b>_TOTAL_</b> mesas',
                'sInfoEmpty': '',
                'sInfoFiltered': '(filtrado de un total de _MAX_ mesa)',
                'sInfoPostFix': '',
                'sSearch': 'Buscar:',
                'sUrl': '',
                'sInfoThousands': ',',
                'sLoadingRecords': 'Cargando...',
                'oPaginate': {
                    'sFirst': 'Primero',
                    'sLast': 'Último',
                    'sNext': 'Siguiente',
                    'sPrevious': 'Anterior'
                },
                'oAria': {
                    'sSortAscending': ': Activar para ordenar la columna de manera ascendente',
                    'sSortDescending': ': Activar para ordenar la columna de manera descendente'
                }
            },
        })
    </script>
@endsection
