<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/create_table', 'App\Http\Controllers\API\APITableController@createTable');
Route::post('/delete_table', 'App\Http\Controllers\API\APITableController@deleteTable');
Route::post('/check_disponibility', 'App\Http\Controllers\API\APIReservationController@checkDisponibility');
Route::post('/create_reservation', 'App\Http\Controllers\API\APIReservationController@createReservation');
Route::post('/delete_reservation', 'App\Http\Controllers\API\APIReservationController@deleteReservation');
