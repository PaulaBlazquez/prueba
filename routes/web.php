<?php

use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('manage_tables', [\App\Http\Controllers\TableController::class, 'index']);
Route::post('create_table', [\App\Http\Controllers\TableController::class, 'createTable']);
Route::post('delete_table', [\App\Http\Controllers\TableController::class, 'deleteTable']);

Route::get('manage_reservations', [\App\Http\Controllers\ReservationController::class, 'index']);
Route::get('dataTable', [\App\Http\Controllers\ReservationController::class, 'dataTable']);
Route::post('check_disponibility', [\App\Http\Controllers\ReservationController::class, 'checkDisponibility']);
Route::post('create_reservation', [\App\Http\Controllers\ReservationController::class, 'createReservation']);
Route::post('delete_reservation', [\App\Http\Controllers\ReservationController::class, 'deleteReservation']);
