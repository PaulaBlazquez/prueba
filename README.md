# Sobre el proyecto:
##Prueba Paula Blázquez CoverManager
Este proyecto es la prueba de Paula Blázquez para Covermanager.

En este proyecto se han desarrollado una interfaz para el usuario donde poder
gestionar mesas y reservas, y también se ha desarrollado una API.

----------------------------
En la parte de gestión de mesas se puede ser un listado de las mesas del
restaurante. Con los botones de creación y eliminación de mesas.

Tanto la creación como eliminación de mesas se despliega en dos modales,
con los requisitos solicitados en la prueba.

En la parte de reservas se verá un listado de reservas, ordenadas por día de reserva automaticamente.
En este caso contará con tres botones, uno para ver disponibilidad de mesas para hacer la reserva,
otro para crear la reserva y un ultimo para eliminarlas.

Estas reservas llevarán un id único, que se creará de forma random para poder eliminarlo o darselo al usuario
sin poner en riesgo la integración de la base de datos.

-----------

La api consta de 5 endpoints, todos estos endpoints son con método POST:
- http://127.0.0.1:8000/api/create_table
- http://127.0.0.1:8000/api/delete_table
- http://127.0.0.1:8000/api/check_disponibility
- http://127.0.0.1:8000/api/create_reservation
- http://127.0.0.1:8000/api/delete_reservation

###api/create_table : POST
Este endpoint espera los datos de:
- minimum_capacity -> numérico
- maximum_capacity -> numérico

Devuelve las mesas que hay en la base de datos con toda su información 
y un mensaje de creación completa.

### api/delete_table : POST

Este endpoint espera el dato de:
- table_id -> numérico

Devuelve un mensaje de mesa eliminada correctamente, y la información de la mesa eliminada.

###api/check_disponibility : POST

En este caso, el endpoint espera una fecha y un número de comensales.
- check_date -> fecha en formato dd-mm-yyyy o yyyy-mm-dd
- check_person -> numérico

Te devuelve toda la información de las mesas que estan disponibles para esa fecha y esos comensales.

###api/create_reservation : POST

El endpoint de crear reservas espera los datos de:
- reservation_date -> fecha en formato dd-mm-yyyy o yyyy-mm-dd
- people_reservation -> numérico
- table_id -> numérico
- client_name -> string

Te devuelve un id de la reserva, que se genera de forma automática y es único, junto con un mensaje de Reserva creada correctamente.

###api/delete_reservation : POST
Este endpoint el único dato que recibe es:

- reservation_id -> string

Este reservation_id que se espera es el que se crea en el endpoint anterior de crear reserva,
es decir, el que se genera automáticamente.


## Construido con:

 - PHP 7.4
 - Laravel 8.0
 - Bootstrap 4.0
 - JQuery 3.2 y 3.5
 - DataTables 1.10
 - InfyOm 
 - Postman

###Información extra sobre paquetes usados
- https://datatables.net/examples/styling/bootstrap4.html
- https://github.com/yajra/laravel-datatables
- https://getbootstrap.com/docs/4.0/getting-started/introduction/#quick-start
- https://www.infyom.com/open-source/laravelgenerator/docs/8.0/installation#installing-into-existing-laravel-projects

#Para empezar
##Prerequisitos
Para ejecutar este proyecto deberás tener instalado:
- PHP
- Composer
- MySQL
- Apache
- Postman

##Instalación

Al descargarte este proyecto, deberás seguir estos pasos para poderlo poner en marcha.
Todos estos pasos son desde la terminal, dentro del proyecto, para acceder a las carpetas se usa `cd carpeta`

###Interfaz web

1. El primer paso es imprescindible para que se descarguen todas las dependencias.

    `composer install`

2. Deberás crear una base de datos con el nombre que tu quieras, si usas la consola 
   deberás acceder primero a mysql con 
   
   `$mysql -u usuario -p`
   
    una vez dentro, ya podrás crear la base de datos con:
   
    `CREATE DATABASE nombredelatabla`
   

3. Seguidamente deberás configurar tu archivo `.env`, de la línea 10 a la 15 tendrás que hacer cambios en:
   
   DB_DATABASE='nombre de la base de datos que hayas creado en el paso 2'
   
   DB_USERNAME='usuario con el que te conectes a la base de datos'
   
   DB_PASSWORD='contraseña con la que te conectes a la base de datos'

_*Si no encuentras este archivo, puedes hacer una copia del archivo `.env.example` y cambiar las 
líneas dichas anteriormente._

4. Vamos a generar una clave del proyecto con el siguiente comando:
   `php artisan key:generate`


5. Una vez tengas tu base de datos conectada, deberás usar el comando
   
    `php artisan migrate`

    este comando te creará las tablas con sus relaciones correspondientes.
   

6. Para generar los datos automáticos de las mesas requeridas en la prueba usaremos:

    `php artisan db:seed`


7. Por último para arrancar el proyecto y poder probarlo usaremos:
   
    `php artisan serve`, ya podremos buscar en nuestro navegador 127.0.0.1:8000 o localhost:8000

###API
1. El primer paso es imprescindible para que se descarguen todas las dependencias.

   `composer install`

2. Deberás crear una base de datos con el nombre que tu quieras, si usas la consola
   deberás acceder primero a mysql con

   `$mysql -u usuario -p`

   una vez dentro, ya podrás crear la base de datos con:

   `CREATE DATABASE nombredelatabla`


3. Seguidamente deberás configurar tu archivo `.env`, de la línea 10 a la 15 tendrás que hacer cambios en:

   DB_DATABASE='nombre de la base de datos que hayas creado en el paso 2'

   DB_USERNAME='usuario con el que te conectes a la base de datos'

   DB_PASSWORD='contraseña con la que te conectes a la base de datos'

_*Si no encuentras este archivo, puedes hacer una copia del archivo `.env.example` y cambiar las
líneas dichas anteriormente._

4. Vamos a generar una clave del proyecto con el siguiente comando:
   `php artisan key:generate`


5. Una vez tengas tu base de datos conectada, deberás usar el comando

   `php artisan migrate`

   este comando te creará las tablas con sus relaciones correspondientes.


6. Para generar los datos automáticos de las mesas requeridas en la prueba usaremos:

   `php artisan db:seed`

7. Para arrancar el proyecto y poder probarlo usaremos:

   `php artisan serve`

_Es muy importante que no cerremos la consola donde se este ejucutando este proceso_


8. Debemos acceder a nuestra aplicación que nos permite tirar peticiones contra una API.
En mi caso uso POSTMAN.
   
    8.1. Para tirar peticiones en postman debemos cambiar el metodo GET por POST 

    8.2. Debemos apuntar nuestro endpoint hacia local, podemos usar `http://` seguido de `127.0.0.1:8000` o bien `localhost:8000`

    8.3. Por último deberemos rellenar el endpoint con los puestos anteriormente, por ejemplo `/api/check_disponibility`
   
    ![img.png](img.png)
    

#Autores
- **Paula Blázquez Wnuk**.
